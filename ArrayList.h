#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H

#include <ostream>

template <class T>
struct ArrayList {
    T* arr;
    int count;
    int capacity;

    ArrayList() {
        count = 0;
        capacity = 1;
        arr = new T[capacity];
    }

    void inflate() {
        if (count == capacity) {
            capacity *= 2;

            T* temp = new T[capacity];
            for (int i = 0; i < count; i++) {
                temp[i] = arr[i];
            }

            T* old = arr;
            arr = temp;
            delete[] old;
        }
    }

    void deflate() {
        if (count * 2 < capacity) {
            capacity /= 2;

            T* temp = new T[capacity];
            for (int i = 0; i < count; i++) {
                temp[i] = arr[i];
            }

            T* old = arr;
            arr = temp;
            delete[] old;
        }
    }

    void append(T value) {
        arr[count] = value;
        count++;

        inflate();
    }


    void prepend(T value) {
        for (int i = count; i > 0; i--) {
            arr[i] = arr[i - 1];
        }
        arr[0] = value;
        count++;

        inflate();
    }

    T removeFirst() {
        if (count == 0){
            throw std::logic_error("Empty List");
        }

        T value = arr[0];

        for (int i = 0; i < count - 1; i++) {
            arr[i] = arr[i + 1];
        }

        count--;
        deflate();

        return value;
    }

    T removeLast() {
        if (count == 0){
            throw std::logic_error("Empty List");
        }

        T value = arr[count - 1];
    
        count--;
        deflate();

        return value;
    }

    bool isEmpty() {
        return count == 0;
    }
};


template <class T>
std::ostream& operator<<(std::ostream& os, ArrayList<T>& list) {
    os << "[";
    for (int i = 0; i < list.count; i++) {
        os << list.arr[i];
        if (i < list.count - 1) {
            os << ", ";
        }
    }
    os << "]";
    return os;
}

#endif