#include <iostream>
#include "ArrayList.h"
#include "TimeSupport.h"
using namespace std;

int main() {
    const int N = 50000;

    ArrayList<int> list;

    timestamp startAdd = current_time();
    for (int i = 0; i < N; i++) {
        list.append(0);
    };
    timestamp endAdd = current_time();
    int addDuration = time_diff(startAdd, endAdd);
    cout << "Add Duration:    " << addDuration << " ms" << endl;
    
    timestamp startRemove = current_time();
    for (int i = 0; i < N; i++) {
        list.removeLast();
    };
    timestamp endRemove = current_time();
    int removeDuration = time_diff(startRemove, endRemove);
    cout << "Remove Duration: " << removeDuration << " ms" << endl;

    return 0;
}