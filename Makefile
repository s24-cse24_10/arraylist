all:
	g++ main.cpp -I. -o app

.PHONY: test
test:
	g++ -g -I. test.cpp -o test
	clear && echo "Running all tests:" && ./test --output=color

clean:
	rm -f app test temp

.PHONY: pull
pull:
ifeq ($(shell git rev-parse --is-inside-work-tree 2>/dev/null),true)
	@git reset --hard
	@git clean -fdx
	@git pull --ff-only
else
	@echo "No git repository found in the current directory"
endif
